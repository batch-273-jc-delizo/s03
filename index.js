/*
QUIZ
1. Class
2. PascalCase
3. new
4. Instantiation
5. constructor
*/

// ACTIVITY

// 1. 

/*class Student {
  constructor(name, email, grades) {
    this.name = name;
    this.email = email;
    if (Array.isArray(grades) && grades.length === 4 &&
        grades.every((grade) => grade >= 0 && grade <= 100)) {
      this.grades = grades;
    } else {
      this.grades = undefined;
    }
  }
}*/

class Student {
  constructor(name, email, grades) {
    this.name = name;
    this.email = email;
    if (Array.isArray(grades) && grades.length === 4 &&
        grades.every((grade) => grade >= 0 && grade <= 100)) {
      this.grades = grades;
    } else {
      this.grades = undefined;
    }
    this.passed = undefined;
    this.passedWithHonors = undefined;
  }
  
  willPass() {
    if (this.grades && this.grades.reduce((sum, grade) => sum + grade, 0) / 4 >= 85) {
      console.log(`${this.name} will pass.`);
      this.passed = true; // Set passed property to true if the student passed
    } else {
      console.log(`${this.name} will not pass.`);
      this.passed = false; // Set passed property to false if the student did not pass
    }
    
    return this; // Return the object instance (i.e., `this`) at the end of the method execution
  }
  
  willPassWithHonors() {
    if (this.grades && this.grades.reduce((sum, grade) => sum + grade, 0) / 4 >= 90) {
      console.log(`${this.name} will pass with honors.`);
      this.passedWithHonors = true; // Set passedWithHonors property to true if the student passed with honors
    } else {
      console.log(`${this.name} will not pass with honors.`);
      this.passedWithHonors = false; // Set passedWithHonors property to false if the student did not pass with honors
    }
    
    return this; // Return the object instance (i.e., `this`) at the end of the method execution
  }
}




let studentFive = new Student('Eris', 'eris@mail.com', [89, 84, 78, 88]);
let studentSix = new Student('Manganelo', 'nelo@mail.com', [101, 82, 79, 85]);
let studentSeven = new Student('Leonard', 'leo@mail.com', [-10, 89, 91, 93]);


// 2. 

let studentOne = new Student('John', 'john@mail.com', [89, 84, 78, 88]);
let studentTwo = new Student('Joe', 'joe@mail.com', [78, 82, 79, 85]);
let studentThree = new Student('Jane', 'jane@mail.com', [87, 89, 91, 93]);
let studentFour = new Student('Jessie', 'jessie@mail.com', [91, 89, 92, 93]);


/*
QUIZ II
1. No, class methods should not be included in the class constructor.
2. Yes, class methods can be separated by commas.
3. Yes, you can update an object's properties via dot notation.
4. The methods used to regulate access to an object's properties are known as accessors or getter and setter methods.
5. In order for a method to be chainable, it needs to return the object instance (i.e., this) at the end of its execution.
*/

// ACTIVITY II
// 1. 

